---
title: How to videos
date: 2019-02-09
---


### Fedilab in action with Peertube
<iframe width="560" height="315" sandbox="allow-same-origin allow-scripts" src="https://peertube.fedilab.app/videos/embed/47b45632-17d3-4d0c-8ff8-f6695b9a27bc" frameborder="0" allowfullscreen></iframe>

A video showing Fedilab in action with Peertube. I didn't show the notifications but they work.

### How to mention people from your contact adresses
<iframe width="560" height="315" sandbox="allow-same-origin allow-scripts" src="https://peertube.fedilab.app/videos/embed/3c1626a4-7747-4d16-8c4b-af34282580f6" frameborder="0" allowfullscreen></iframe>

Small video (uploaded via Fedilab) showing how the new address book works on Fedilab.

### how to upload a video on Peertube with Fedilab
<iframe width="560" height="315" sandbox="allow-same-origin allow-scripts" src="https://peertube.fedilab.app/videos/embed/9943aa6a-2ed0-4871-aecd-1f5d152d2b89" frameborder="0" allowfullscreen></iframe>

This video has been uploaded on #Peertube with #Fedilab

### How to delete and re-draft
<iframe width="560" height="315" sandbox="allow-same-origin allow-scripts" src="https://peertube.fedilab.app/videos/embed/68f1ab1d-5eef-4d81-81b7-48f59085d954" frameborder="0" allowfullscreen></iframe>

Short video showing how to use the delete and re-draft feature with Fedilab

### How to translate a message before posting it
<iframe width="560" height="315" sandbox="allow-same-origin allow-scripts" src="https://peertube.fedilab.app/videos/embed/a02c7e57-9bac-45d7-9e65-9e01533e9f5c" frameborder="0" allowfullscreen></iframe>

A short video showing how to translate a toot before posting it. Start to compose it in your language, then pick up the targeted language.

### How to do cross-account actions
<iframe width="560" height="315" sandbox="allow-same-origin allow-scripts" src="https://peertube.fedilab.app/videos/embed/43b004a8-3738-4109-81b3-c94f62825284" frameborder="0" allowfullscreen></iframe>

A short video showing how to use cross-account actions wirh long press.

### How to mention other people in a thread
<iframe width="560" height="315" sandbox="allow-same-origin allow-scripts" src="https://peertube.fedilab.app/videos/embed/37ca6cb3-b9ea-4461-9825-f5d96a2ca90a" frameborder="0" allowfullscreen></iframe>

Short video showing how to quickly add mentions in a conversation for a common reply.

### How to use timed mute
<iframe width="560" height="315" sandbox="allow-same-origin allow-scripts" src="https://peertube.fedilab.app/videos/embed/4eb64bfc-71a4-4dcf-ac21-baeb2ced3143" frameborder="0" allowfullscreen></iframe>

A short video showing how to use the timed mute feature on Fedilab. An account will be muted until a date or if it is disabled on the profile.

### How to find new people to follow
<iframe width="560" height="315" sandbox="allow-same-origin allow-scripts" src="https://peertube.fedilab.app/videos/embed/672f188a-0284-4bf8-b259-d16455e1ae9f" frameborder="0" allowfullscreen></iframe>

Fedilab uses the Trunk API to suggest new people to follow and will add them automatically in a list.
About Trunk: "Trunk allows you to mass-follow a bunch of people in order to get started with Mastodon or any other platform on the Fediverse."
More: [https://communitywiki.org/trunk](https://communitywiki.org/trunk)

### How to change icon & text sizes
<iframe width="560" height="315" sandbox="allow-same-origin allow-scripts" src="https://peertube.fedilab.app/videos/embed/3250e043-5c20-49ea-9893-a1b38e2e989a" frameborder="0" allowfullscreen></iframe>

Short video showing how to change icon and text size with Fedilab

### Follow remote instance on Mastodon
<iframe width="560" height="315" sandbox="allow-same-origin allow-scripts" src="https://peertube.fedilab.app/videos/embed/57b08f35-3ab6-4579-8551-345450dce387" frameborder="0" allowfullscreen></iframe>

Short video showing how to follow a remote instance on Mastodon with Fedilab.

