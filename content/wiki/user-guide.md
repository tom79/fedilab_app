*[Home](../home) &gt;* User guide

<div align="center">
<img src="../res/fedilab_icon.png" width="70px">
<b>Fedilab</b>
</div>

## User guide

*Fedilab supports these Fediverse platforms*

• Mastodon • Pleroma • Peertube •<br>
• GNU Social • Friendica • Pixelfed •

**The login procedure is similar for all platforms.**

- [Login](./login)<br><br>
