*[Home](../../home) > [Features](../../features) &gt;* Custom Sharing

<div align="center">
<img src="../../res/fedilab_icon.png" width="70px">
<b>Fedilab</b>
</div>

## Custom Sharing

### Overview

This feature allows the sharing of toots to content aggregation sites.  Content curators can selectively share toots in their feed to a custom configured URL, allowing for a curated news feed that contains Mastodon toots.  

RSS uses metadata that is not perfectly compatible with the Mastodon toot structure, so the user of this feature has the opportunity to edit the suggested Title and Keywords identified in the selected toot, as an opportunity to tailor the Mastodon toot presentation in the affected content aggregator's RSS/Atom feed.

A [Custom Sharing Server project](https://gitlab.com/tom79/custom-sharing-server) has been created to help content creators deploy a content aggregation server, to curate both externally and internally generated content into an RSS feed.  The Custom Sharing Server project uses a token value to both authorize adding content to the server and to identify the user, when content is added to the site.

1. **To activate Custom Sharing in the Fedilab app, click the Custom sharing checkbox in Settings, and a prompt for your Custom Sharing URL will appear with a default value** <br><br> <img src="../../res/custom_share/settings.png" width="300px"><br><img src="../../res/custom_share/settings_2.png" width="300px">

2. **Edit the Custom Sharing URL to be compatible with a content aggregator of your choice.  A URL value suitable for testing the Custom Sharing feature is shown below** <br><br> `http://teci.world/add?user=fedilab&url=${url}&title=${title}&source=${source}&id=${id}&description=${description}&keywords=${keywords}&creator=${creator}&thumbnailurl=${thumbnailurl}`<br><br><img src="../../res/custom_share/settings_3.png" width="300px">

3. **To use the Custom Sharing feature, select a toot, select the ellipsis, then if the toot has public visibility, you will see Custom sharing in the options.  Select Custom sharing in the options list** <br><br> <img src="../../res/custom_share/toot_options.png" width="300px"><br><img src="../../res/custom_share/toot_options_2.png" width="300px">

4. **When Custom sharing is selected, a Custom Sharing window will appear with suggested Title and Keywords fields and a read-only Description field.  Edit Title and Keywords as required, then select Submit, and observe the popup text that indicates the server response.  Note that each server will have its own response format.**  <br><br> <img src="../../res/custom_share/suggested_share.png" width="300px"><br><img src="../../res/custom_share/edited_share.png" width="300px"><br><img src="../../res/custom_share/share_results.png" width="300px">

### Custom Sharing URL Editing

The Custom Sharing URL can be fully edited to match the requirements of the content aggregation server that you are connecting to.  The default Custom Sharing URL is:

`http://example.net/add?token=YOUR_TOKEN&url=${url}&title=${title}&source=${source}&id=${id}&description=${description}&keywords=${keywords}&creator=${creator}&thumbnailurl=${thumbnailurl}`

The variables that the Custom Sharing feature passes to a content aggregator site includes:

*  **${url}** - Auto-extracted publicly accessible URL to the selected toot
*  **${title}** - Auto-suggested and editable Title value for use in the RSS/Atom feed. The Title is suggested from the initial portion of the toot text until either the first line break or 60+ characters are encountered.
*  **${keywords}** - Auto-suggested and editable Keywords value for use in the RSS/Atom feed.  Keywords are suggested from the hashtags in the selected toot.
*  **${description}** - Auto-extracted Description value for use in the RSS/Atom feed
*  **${creator}** - Auto-extracted name of the account that created the toot
*  **${source}** - Auto-extracted URL of the account that created the toot
*  **${thumbnailurl}** - Auto-extracted URL of either a thumbnail from a toot card view, from a media attachment or from the avatar of the account that created the toot
*  **${id}** - Auto-extracted id of the selected toot

The variables must be presented in the the Custom Sharing URL using the **${variable}** format in order to ensure the variable values are properly encoded and delivered to the content aggregation server.  Not all variables need to be used, and the URL parameters can be renamed as required.  Following are some example valid Custom Sharing URL values.

`http://teci.world/add?user=fedilab&url=${url}&title=${title}&source=${source}&id=${id}&description=${description}&keywords=${keywords}&creator=${creator}&thumbnailurl=${thumbnailurl}`

`http://mysite.com/contentinput?sechash=U2L34NC22R0&objtitle=${title}&objurl=${url}&objsrc=${source}&objdesc=${description}&objkw=${keywords}&creator=${creator}&thumbnailurl=${thumbnailurl}`

`http://contentcreator.io/rssinput?title=${title}&url=${url}&desc=${description}&objkw=${keywords}&creator=${creator}&thumbnailurl=${thumbnailurl}`

`http://cs.teci.world/add?token=umVe1zurZk47ihE_S0QcBGO5KUSA2v-GSet4_fFnJ4L&url=${url}&title=${title}&source=${source}&id=${id}&description=${description}&keywords=${keywords}&creator=${creator}&thumbnailurl=${thumbnailurl}`

