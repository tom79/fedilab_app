*[Home](../../home) > [Features](../../features) &gt;* Custom emojis

<div align="center">
<img src="../../res/fedilab_icon.png" width="70px">
<b>Fedilab</b>
</div>

## Custom Emojis
This feature will allow to use custom emojis when writing a toot

- **Custom emojis will be suggested when starting a word with "&nbsp;:&nbsp;" + one character**

  - A list with few results will be displayed <br><br> <img src="../../res/emoji/emoji_1.png" width="300px">

  - To extend these results, swipe on this list <br><br> <img src="../../res/emoji/emoji_2.png" width="300px">


- **You can also choose an emoji through the emoji picker feature:**

  - Click on the smiley icon on top <br><br> <img src="../../res/emoji/emoji_3.png" width="300px">

  - Now you can choose and emoji from the panel <br><br> <img src="../../res/emoji/emoji_4.png" width="300px">
